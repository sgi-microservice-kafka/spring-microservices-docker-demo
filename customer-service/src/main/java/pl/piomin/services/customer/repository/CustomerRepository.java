package pl.piomin.services.customer.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import pl.piomin.services.customer.model.Customer;

public class CustomerRepository {

	private List<Customer> customers = new ArrayList<>();
	
	public Customer add(Customer customer) {
		customer.setId((long) (customers.size()+1));
		customers.add(customer);
		return customer;
	}
	
	public Customer findById(Long id) {
		return customers.stream()
				.filter(a -> a.getId().equals(id))
				.findFirst()
				.orElseThrow();
	}
	
	public List<Customer> findAll() {
		return customers;
	}
	
	public List<Customer> findByDepartment(Long departmentId) {
		return customers.stream()
				.filter(a -> a.getDepartmentId().equals(departmentId))
				.toList();
	}
	
	public List<Customer> findByOrganization(Long organizationId) {
		return customers.stream()
				.filter(a -> a.getOrganizationId().equals(organizationId))
				.toList();
	}
	
}
